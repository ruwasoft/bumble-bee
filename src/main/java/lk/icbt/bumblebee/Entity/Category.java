package lk.icbt.bumblebee.Entity;

public class Category {

    private int ID;
    private String Name;

    public Category() {
    }

    public Category(int ID, String name) {
        this.ID = ID;
        Name = name;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        Name = name;
    }
}
