<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Product</title>

    <style>
        * {
            box-sizing: border-box;
        }

        h1
        {
            padding: 20px;
            background: #fbd31c;
            color: black;
            border-bottom: 1px solid #e0e4e8;
        }

        input[type=text], select, textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }

        select:required:invalid {
            color: #555;
        }

        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
        }

        input[type=submit] {
            width: 10%;
            background-color: black;
            color: #fbd31c;
            font-weight: bold;
            padding: 14px 20px;
            margin: 8px 5px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }

        input[type=submit]:hover {
            background-color: #171717;
            color: white;
        }

        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }

        .col-75 {
            float: left;
            width: 75%;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 600px) {
            .col-25, .col-75, input[type=submit] {
                width: 100%;
                margin-top: 0;
            }
        }

        input[type=button] {
            width: 10%;
            background-color: black;
            color: #fbd31c;
            font-weight: bold;
            padding: 14px 20px;
            margin: 8px 5px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }

        input[type=button]:hover {
            background-color: #171717;
            color: white;
        }


        /*Style for image upload------------------------------------------- */
        .form-input {
            width:350px;
            padding:20px;
            background:#fff;
            box-shadow: -3px -3px 7px rgba(94, 104, 121, 0.377),
            3px 3px 7px rgba(94, 104, 121, 0.377);
        }
        .form-input input {
            display:none;

        }

        .form-input label {
            width: 45%;
            height: 45px;
            margin-left: 0;
            line-height: normal;
            text-align: center;
            background: #fbd31c;
            color: black;
            font-size: 15px;
            font-family: "Open Sans",sans-serif;
            text-transform: Uppercase;
            font-weight: normal;
            border-radius: 5px;
            cursor: pointer;
        }

        .form-input img {
            width:100%;
            height: 120px;
            width: 180px;
            margin-bottom:30px;
        }

        /*End of styles for image upload------------------------------------*/


    </style>


    <script>
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Please enter only numbers");
                return false;
            }
            return true;
        }

        function showPreview(event){
            if(event.target.files.length > 0){
                var src = URL.createObjectURL(event.target.files[0]);
                var preview = document.getElementById("file-ip-1-preview");
                preview.src = src;
                preview.style.display = "block";
            }
        }

    </script>


</head>
<body>

<!--Receive data parsing from previous page------------------->
<%
    String id = request.getParameter("id");
    String name = request.getParameter("name");
    String description = request.getParameter("description");
    String price = request.getParameter("price");
    String cutPrice = request.getParameter("cutPrice");
    String selectedMainCategory = request.getParameter("mainCategory");
    String selectedBrand = request.getParameter("brand");
    String image = request.getParameter("image");
    String availableQty=request.getParameter("availableQty");
%>
<!--End of Receiving data parsing from previous page---------->


<!--Variables set to access in forEach----------------------------------------------------->
<c:set var = "selectedMainCategory" scope = "session" value = "<%=selectedMainCategory%>"/>
<c:set var = "selectedBrand" scope = "session" value = "<%=selectedBrand%>"/>
<!--Ending of Variables set to access in forEach-------------------------------------------->

<h1> Edit Product </h1>


<div class="container">

    <form action="EditProduct" method="post" enctype="multipart/form-data">


        <!--Image upload button-------------------------------------------------------------------------------------------->
        <div class="form-input">

            <div class="preview">

                <img id="file-ip-1-preview" src="<%=image%>">

            </div>

            <label for="file-ip-1">Upload Image</label>
            <input type="file" id="file-ip-1" name="image" accept="image/*" onchange="showPreview(event);">

            <input type="hidden" name="currentImage" value="<%= image %>"> <!--This is a hidden input field. I use this value to parse to update sql, if image not selected.-->

        </div>
        <!--End of Image upload button------------------------------------------------------------------------------------->

        <br>

        <div class="row">
            <div class="col-25">
                <label for="productId">Product ID</label>
            </div>
            <div class="col-75">
                <input type="text" id="productId" name="productId" placeholder="Product ID.." readonly value="<%=id%>" required>
            </div>
        </div>


        <div class="row">
            <div class="col-25">
                <label for="productName">Product Name</label>
            </div>
            <div class="col-75">
                <input type="text" id="productName" name="productName" placeholder="Product name.." value="<%=name %>" required maxlength="45">
            </div>
        </div>



        <div class="row">
            <div class="col-25">
                <label for="description">Product Description</label>
            </div>
            <div class="col-75">
                <input type="text" id="description" name="description" placeholder="Product description.." value="<%=description %>" required maxlength="200">
            </div>
        </div>


        <div class="row">
            <div class="col-25">
                <label for="price">Product Price </label>
            </div>
            <div class="col-75">
                <input type="text" id="price" name="price" placeholder="Product price.." required maxlength="10" inputmode="numeric" value="<%= price%>" onkeypress="return isNumberKey(event)" >
            </div>
        </div>



        <div class="row">
            <div class="col-25">
                <label for="cutPrice">Cut Price</label>
            </div>
            <div class="col-75">
                <input type="text" id="cutPrice" name="cutPrice" placeholder="Cut Price.." value="<%=cutPrice %>" required maxlength="10" inputmode="numeric" onkeypress="return isNumberKey(event)" >
            </div>
        </div>


        <div class="row">
            <div class="col-25">
                <label for="mainCategory">Main Category</label>
            </div>
            <div class="col-75">

                <select id="mainCategory" name="mainCategory" required>

                    <option value=""> Select a category.. </option> <!-- Placeholder option -->

                    <c:forEach var="category" items="${categoryList}">

                        <c:if test="${category.name == selectedMainCategory }">
                            <option value="${category.name}" selected>${category.name}</option>
                        </c:if>

                        <c:if test="${category.name != selectedMainCategory}">
                            <option value="${category.name}">${category.name}</option>
                        </c:if>

                    </c:forEach>

                </select>

            </div>
        </div>


        <div class="row">
            <div class="col-25">
                <label for="brand">Product Brand</label>
            </div>
            <div class="col-75">

                <select  id="brand" name="brand" required>

                    <option value=""> Select a brand.. </option> <!-- Placeholder option -->

                    <c:forEach var = "brand" items="${brandList}">

                        <c:if test="${brand.name == selectedBrand }">
                            <option value="${brand.name}" selected>${brand.name}</option>
                        </c:if>

                        <c:if test="${brand.name != selectedBrand}">
                            <option value="${brand.name}">${brand.name}</option>
                        </c:if>

                    </c:forEach>

                </select>

            </div>
        </div>




        <div class="row">
            <div class="col-25">
                <label for="availableQty">Available Qty</label>
            </div>
            <div class="col-75">
                <input type="text" id="availableQty" name="availableQty" value="<%=availableQty %>" placeholder="Available Qty.." required maxlength="10" inputmode="numeric" onkeypress="return isNumberKey(event)" >
            </div>
        </div>


        <br>


        <input type="submit" name="submit" value="Update">

        <input type="submit" name="delete" value="Delete" onclick="return confirm('Are you sure you want to delete this product ?');" >

        <input type="button" name="cancel" value="Cancel" onclick="window.location.href='Products'">

    </form>

    <br> <br>

</div>


</body>
</html>
