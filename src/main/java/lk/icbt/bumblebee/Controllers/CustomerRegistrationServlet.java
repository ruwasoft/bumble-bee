package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Common.Common;
import lk.icbt.bumblebee.Entity.Admin;
import lk.icbt.bumblebee.Entity.Customer;
import lk.icbt.bumblebee.Models.AdminDBUtill;
import lk.icbt.bumblebee.Models.CustomerDBUtill;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "CustomerRegistrationServlet", value = "/CustomerRegistrationServlet")
public class CustomerRegistrationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        Customer customer = new Customer();
        customer.setName(request.getParameter("name"));
        customer.setEmail(request.getParameter("email"));
        customer.setPassword(request.getParameter("password"));
        customer.setNIC(request.getParameter("nic"));
        customer.setAddress(request.getParameter("address"));
        customer.setPhone(request.getParameter("phone"));
        customer.setDOB(request.getParameter("DOB"));
        customer.setLoanBalance("0");
        customer.setInstallmentPlan("Not Selected");
        customer.setID(0);

        boolean isTrue = CustomerDBUtill.insertCustomer(customer);

        if(isTrue)
        {

            // Send a success message as an alert box to the user
            out.println("<html>");
            out.println("<head>");
            out.println("<title>MyServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script>");
            out.println("alert('Registration Successful!, Please use mobile app to get the shopping experience');");
            out.println("window.location.replace('index.jsp');");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");


        }
        else
        {

            // Send a err message as an alert box to the user
            out.println("<html>");
            out.println("<head>");
            out.println("<title>MyServlet</title>");
            out.println("<style>");
            out.println("  .alert {");
            out.println("    color: red;");
            out.println("  }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script>");
            out.println("alert('Something went to wrong! Please try again later');");
            out.println("window.location.replace('index.jsp');");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");

        }


    }
}
