package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Product;
import lk.icbt.bumblebee.Models.ProductsDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "CustomerProductsServlet", value = "/CustomerProductsServlet")
public class CustomerProductsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Product> productList = ProductsDBUtill.getProductList();
        request.setAttribute("productList",productList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("CustomerProducts.jsp");
        requestDispatcher.forward(request,response);

    }


}
