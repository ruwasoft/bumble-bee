package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Entity.Category;
import lk.icbt.bumblebee.Models.BrandsDBUtill;
import lk.icbt.bumblebee.Models.CategoryDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminCategoriesServlet", value = "/AdminCategoriesServlet")
public class AdminCategoriesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Category> categoryList = CategoryDBUtill.getCategoryList();
        request.setAttribute("categoryList",categoryList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminCategories.jsp");
        requestDispatcher.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String categoryName = request.getParameter("categoryName");

        boolean isTrue;

        isTrue = CategoryDBUtill.insertCategory(categoryName);

        if(isTrue == true)
        {
            // Redirect to AdminCategoriesServlet
            response.sendRedirect(request.getContextPath() + "/Categories");
        }
        else
        {
            response.setContentType("text/html");
            response.getWriter().write("<script>alert('An error has occurred');</script>");

            // Redirect to AdminCategoriesServlet
            response.sendRedirect(request.getContextPath() + "/Categories");

        }

    }
}
