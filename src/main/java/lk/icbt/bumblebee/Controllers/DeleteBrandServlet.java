package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Models.BrandsDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "DeleteBrandServlet", value = "/DeleteBrandServlet")
public class DeleteBrandServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String brandID = request.getParameter("id");

        boolean isTrue;
        isTrue = BrandsDBUtill.deleteBrand(brandID);

        if(isTrue == true)
        {

            // Redirect to AdminBrandsServlet
            response.sendRedirect(request.getContextPath() + "/Brands");

        }
        else
        {
            response.setContentType("text/html");
            response.getWriter().write("<script>alert('An error has occurred');</script>");

            // Redirect to AdminBrandsServlet
            response.sendRedirect(request.getContextPath() + "/Brands");

        }

    }
}
