# Bumble Bee - Buy First and Pay Later

* This is a simple admin panel web application created using jsp and servlets with my SQL database. 
* Please create a database in your local machine by opening 'BumbleBeeDB.mwb' file in 'database template' folder from MySQL Workbench.
* I used 'XAMP' as the database server.

<br>

### Two users in this system
1. Customer 
    * Register
    * Login
    * View Products
    * View own Profile details
   
2. Admin
    * Login
    * View/Add/Update/Delete Products
    * View/Add/Delete Categories
    * View/Add/Delete Brands
    * View registered customers with details
    * View own profile details


## Customer registration page

* This is a first page of and application.

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/cusRegPage.png)


## Customer Login Page

* Registered customers can login to the system

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/cusLoginPage.png)


## Customer dashboard

* Customer will navigate to this page after the login successful. Only he can view products available here and view his own profile details.

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/cusHome.png)


## Admin login page

* You can navigate to this page by clicking a 'You are an admin ?' label in a customer registration page

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/2.png)


## Admin Home Page

* Admin will navigate to this page after the successful login

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/3.png)



## Products page

* You can see the all products in here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/4.png)




## Add New products page

* You can add new products to the DB from here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/11.png)



## Edit products page

* You can edit all product details from here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/10.png)



## Categories page

* You can add a new category or delete a current category from here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/5.png)



## Brands page

* You can add a new brand or delete a current brand from here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/6.png)




## Customers page

* You can view all registered customer details in here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/7.png)


## Profile page

* Logged in admin details are showing in here

![N|Solid](https://gitlab.com/ruwasoft/bumble-bee/-/raw/main/screenShots/8.png)


