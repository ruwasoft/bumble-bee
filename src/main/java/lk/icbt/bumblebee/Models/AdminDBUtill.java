package lk.icbt.bumblebee.Models;

import lk.icbt.bumblebee.Database.DBConnect;
import lk.icbt.bumblebee.Entity.Admin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdminDBUtill {

    private static boolean isSuccess = false;
    private static Connection con = null;
    private  static Statement stmt = null;
    private static ResultSet rs= null;



    //Function use to login---------------------------------------------------------------------------------------------
    public static boolean AdminLogin(String email, String password)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "SELECT * FROM TblAdmin WHERE Email='"+email+"' AND Password = '"+password+"' ";
            rs = stmt.executeQuery(sql);

            if(rs.next())
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


        return isSuccess;

    }
    //End of function use to login--------------------------------------------------------------------------------------



    //Function use to get admin details---------------------------------------------------------------------------------
    public static List<Admin> getAdminDetails(String email)
    {
        ArrayList<Admin> admins = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TBLAdmin where Email= '"+email+"' ";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                String password = rs.getString(2);
                String name = rs.getString(3);
                String phone = rs.getString(4);


                Admin admin = new Admin(email,password,name,phone);

                admins.add(admin);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  admins;

    }
    //End oif function use to get admin details-------------------------------------------------------------------------


}
