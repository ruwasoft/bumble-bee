<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>

    <title>Home</title>
    <link rel="stylesheet" href="css/adminDashboardStyle.css">
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>

    <style>
        .square {
            width: 250px;
            height: 100px;
            border: 1px solid black;
            display: inline-block;
            text-align: center;
            font-size: 24px;
            font-weight: bold;
            line-height: 100px;
            background-color: #fbd31c;
        }
    </style>

</head>

<body>


<c:set var="productsCount" value="${requestScope.productsCount}" />
<c:set var="categoriesCount" value="${requestScope.categoriesCount}" />
<c:set var="brandsCount" value="${requestScope.brandsCount}" />
<c:set var="customersCount" value="${requestScope.customersCount}" />



<div class="wrapper">

    <jsp:include page="AdminSideBar.jsp" />

    <div class="main_content">
        <div class="header"> Welcome!! Have a nice day.</div>
        <div class="info">


            <!-- Show Counts cards------------------------->
            <div class="square">
                Products :
                ${productsCount}
            </div>
            <div class="square">
                Categories :
                ${categoriesCount}
            </div>
            <div class="square">
                Brands :
                ${brandsCount}
            </div>
            <div class="square">
                Customers :
                ${customersCount}
            </div>
            <!--End of Show Counts cards------------------->

            <br> <br> <br> <br>

            <img src="img/logo.png">



        </div>
    </div>

</div>





</body>
</html>






