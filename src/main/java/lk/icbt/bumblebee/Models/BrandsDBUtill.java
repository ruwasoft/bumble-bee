package lk.icbt.bumblebee.Models;

import lk.icbt.bumblebee.Database.DBConnect;
import lk.icbt.bumblebee.Entity.Brand;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BrandsDBUtill {


    private static boolean isSuccess = false;
    private static Connection con = null;
    private  static Statement stmt = null;
    private static ResultSet rs= null;


    //Function use to get All brand details---------------------------------------------------------------------------------
    public static List<Brand> getBrandList()
    {
        ArrayList<Brand> brands = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TblBrands";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                int id = rs.getInt(1);
                String name = rs.getString(2);

                Brand brand = new Brand(id,name);

                brands.add(brand);

            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  brands;

    }
    //End oif function use to get brand details-------------------------------------------------------------------------


    ///Function use to insert-------------------------------------------------------------------------------------------
    public static boolean insertBrand(String brandName)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "INSERT INTO TblBrands VALUES (0,'"+brandName+"')";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return isSuccess;
    }
    ///End of Function use to insert------------------------------------------------------------------------------------


    ///function use to delete the brand from db----------------------------------------------------------------------
    public static boolean deleteBrand(String id)
    {
        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            int convertedID = Integer.parseInt(id);

            String sql = "DELETE FROM TblBrands  WHERE ID='"+convertedID+"' ";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  isSuccess;
    }
    ///End of function use to delete the brand from db---------------------------------------------------------------

}
