<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Add New Product</title>

  <style>
    * {
      box-sizing: border-box;
    }

    h1
    {
      padding: 20px;
      background: #fbd31c;
      color: black;
      border-bottom: 1px solid #e0e4e8;
    }

    input[type=text], select, textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }

    select:required:invalid {
      color: #555;
    }

    label {
      padding: 12px 12px 12px 0;
      display: inline-block;
    }

    input[type=submit] {
      width: 10%;
      background-color: black;
      color: #fbd31c;
      font-weight: bold;
      padding: 14px 20px;
      margin: 8px 5px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }

    input[type=submit]:hover {
      background-color: #171717;
      color: white;
    }

    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }

    .col-25 {
      float: left;
      width: 25%;
      margin-top: 6px;
    }

    .col-75 {
      float: left;
      width: 75%;
      margin-top: 6px;
    }

    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
      }
    }


    input[type=button] {
      width: 10%;
      background-color: black;
      color: #fbd31c;
      font-weight: bold;
      padding: 14px 20px;
      margin: 8px 5px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
    }

    input[type=button]:hover {
      background-color: #171717;
      color: white;
    }

    /*Style for image upload------------------------------------------- */
    .form-input {
      width:350px;
      padding:20px;
      background:#fff;
      box-shadow: -3px -3px 7px rgba(94, 104, 121, 0.377),
      3px 3px 7px rgba(94, 104, 121, 0.377);
    }
    .form-input input {
      display:none;

    }

    .form-input label {
      width: 45%;
      height: 45px;
      margin-left: 0;
      line-height: normal;
      text-align: center;
      background: #fbd31c;
      color: black;
      font-size: 15px;
      font-family: "Open Sans",sans-serif;
      text-transform: Uppercase;
      font-weight: normal;
      border-radius: 5px;
      cursor: pointer;
    }

    .form-input img {
      width:100%;
      display:none;
      margin-bottom:30px;
    }

    /*End of styles for image upload------------------------------------*/

  </style>

  <script>
    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        alert("Please enter only numbers");
        return false;
      }
      return true;
    }

    function showPreview(event){
      if(event.target.files.length > 0){
        var src = URL.createObjectURL(event.target.files[0]);
        var preview = document.getElementById("file-ip-1-preview");
        preview.src = src;
        preview.style.display = "block";
      }
    }

  </script>

</head>

<body>



<h1> Add New Product </h1>


<div class="container">

  <form action="AddNewProduct" method="post" enctype="multipart/form-data">


    <!--Image upload button-------------------------------------------------------------------------------------------->
    <div class="form-input">

      <div class="preview">

        <img id="file-ip-1-preview">

      </div>

      <label for="file-ip-1">Upload Image</label>
      <input type="file" id="file-ip-1" name="image" accept="image/*" onchange="showPreview(event);" required>

    </div>
    <!--End of Image upload button------------------------------------------------------------------------------------->


    <br>


    <div class="row">
      <div class="col-25">
        <label for="productName">Product Name</label>
      </div>
      <div class="col-75">
        <input type="text" id="productName" name="productName" placeholder="Product name.." required maxlength="45">
      </div>
    </div>



    <div class="row">
      <div class="col-25">
        <label for="description">Product Description</label>
      </div>
      <div class="col-75">
        <input type="text" id="description" name="description" placeholder="Product description.." required maxlength="200">
      </div>
    </div>


    <div class="row">
      <div class="col-25">
        <label for="price">Product Price </label>
      </div>
      <div class="col-75">
        <input type="text" id="price" name="price" placeholder="Product price.." required maxlength="10" inputmode="numeric" onkeypress="return isNumberKey(event)" >
      </div>
    </div>



    <div class="row">
      <div class="col-25">
        <label for="cutPrice">Cut Price</label>
      </div>
      <div class="col-75">
        <input type="text" id="cutPrice" name="cutPrice" placeholder="Cut Price.." required maxlength="10" inputmode="numeric" onkeypress="return isNumberKey(event)" >
      </div>
    </div>


    <div class="row">
      <div class="col-25">
        <label for="mainCategory">Main Category</label>
      </div>
      <div class="col-75">
        <select  id="mainCategory" name="mainCategory" required>

          <option value="" selected disabled>Select a category..</option> <!-- Placeholder option -->

          <c:forEach var = "category" items="${categoryList}">
            <option value="${category.name}">${category.name}</option>
          </c:forEach>

        </select>
      </div>
    </div>



    <div class="row">
      <div class="col-25">
        <label for="brand">Product Brand</label>
      </div>
      <div class="col-75">
        <select  id="brand" name="brand" required>

          <option value="" selected disabled>Select a brand..</option> <!-- Placeholder option -->

          <c:forEach var = "brand" items="${brandList}">
            <option value="${brand.name}">${brand.name}</option>
          </c:forEach>

        </select>
      </div>
    </div>




    <div class="row">
      <div class="col-25">
        <label for="availableQty">Available Qty</label>
      </div>
      <div class="col-75">
        <input type="text" id="availableQty" name="availableQty" placeholder="Available Qty.." required maxlength="10" inputmode="numeric" onkeypress="return isNumberKey(event)" >
      </div>
    </div>


    <br>


    <input type="submit" name="submit" value="Add">

    <input type="button" name="cancel" value="Cancel" onclick="window.location.href='Products'">

  </form>

  <br> <br>

</div>


</body>
</html>
