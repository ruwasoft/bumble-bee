<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <title>Products</title>
  <link rel="stylesheet" href="css/adminDashboardStyle.css">
  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>


  <style>
    #products {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #products td, #products th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #products tr:nth-child(even){background-color: #f2f2f2;}

    #products tr:hover {background-color: #ddd;}

    #products th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #fcdc49;
      color: black;
    }

    <!-- -->

    label {
      font-weight: bold;
    }

    input[type=text] {
      width: 20%;
      padding: 12px 20px;
      margin: 8px 4px;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    input[type=submit] {
      width: 10%;
      background-color: black;
      color: #fbd31c;
      font-weight: bold;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      border-radius: 4px;
      cursor: pointer;
    }

    input[type=submit]:hover {
      background-color: #171717;
      color: white;
    }



  </style>

</head>

<body>


<div class="wrapper">

  <jsp:include page="AdminSideBar.jsp" />

  <div class="main_content">
    <div class="header">Products</div>
    <div class="info">

      <!-- Show Products list load from table and show button to add new product -------------------------------------->
      <div>


        <!--Button to add new Product------------------------------------->
        <form action="AddNewProduct">
          <input type="submit" value="Add">
        </form>
        <!--End of Button to add new Product------------------------------>


        <!-- View Products Table here ------------------------------------>
        <table id="products">

          <tr>
            <th>Product Image</th>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Cut Price</th>
            <th>Main Category</th>
            <th>Brand</th>
            <th>Available Qty</th>
            <th></th>
          </tr>

          <c:forEach var = "product" items="${productList}">


            <c:set var="id" value="${product.ID}"/>
            <c:set var="name" value="${product.name}"/>
            <c:set var="description" value="${product.description}"/>
            <c:set var="price" value="${product.price}"/>
            <c:set var="cutPrice" value="${product.cuttedPrice}"/>
            <c:set var="maincategory" value="${product.mainCategory}"/>
            <c:set var="brand" value="${product.brand}"/>
            <c:set var="image" value="${product.image}"/>
            <c:set var="availableQty" value="${product.availableQty}"/>



            <%--Create link for parse data to update page------------%>
            <c:url value="EditProduct" var="EditProduct">
              <c:param name="id" value="${id}"/>
              <c:param name="name" value="${name}"/>
              <c:param name="description" value="${description}"/>
              <c:param name="price" value="${price}"/>
              <c:param name="cutPrice" value="${cutPrice}"/>
              <c:param name="mainCategory" value="${maincategory}"/>
              <c:param name="brand" value="${brand}"/>
              <c:param name="image" value="${image}"/>
              <c:param name="availableQty" value="${availableQty}"/>
            </c:url>
            <%--End of Creating link for parse data to update page --%>


            <tr>

              <td>

                <img src="${image}" height="120" width="180">

              </td>

              <td>${id}</td>
              <td>${name}</td>
              <td>${description}</td>
              <td>${price}</td>
              <td>${cutPrice}</td>
              <td>${maincategory}</td>
              <td>${brand}</td>
              <td>${availableQty}</td>
              <td> <a href="${EditProduct}"> <img src="img/edit.png" height="20" alt="Edit"> </a> </td>
            </tr>


          </c:forEach>

        </table>
        <!-- End of showing products Table ------------------------->

      </div>
      <!--End of Showing Products list load from table and show button to add new product ----------------------------->

    </div>
  </div>

</div>


</body>
</html>
