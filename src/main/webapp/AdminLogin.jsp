
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>
    <title>Admin Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <!-- IMAGE CONTAINER BEGIN -->
        <div class="col-lg-6 col-md-6 d-none d-md-block infinity-image-container">

            <img src="img/bg.png">

        </div>
        <!-- IMAGE CONTAINER END -->

        <!-- FORM CONTAINER BEGIN -->
        <div class="col-lg-6 col-md-6 infinity-form-container">
            <div class="col-lg-9 col-md-12 col-sm-8 col-xs-12 infinity-form">
                <!-- Company Logo -->
                <div class="text-center mb-3 mt-5">
                    <img src="img/logo.png" width="150px">
                </div>
                <div class="text-center mb-4">
                    <h4>Admin Login</h4>
                </div>

                <!-- Form -->
                <form action="AdminLogin" method="post" class="px-3">


                    <!-- Input Box -->
                    <div class="form-input">
                        <span><i class="fa fa-envelope"></i></span>
                        <input type="email" name="Email" placeholder="Email Address" tabindex="10"required>
                    </div>


                    <div class="form-input">
                        <span><i class="fa fa-lock"></i></span>
                        <input type="password" name="Password" id="password" placeholder="Password" required>
                    </div>


                    <div class="row mb-3">
                        <!--Show password Checkbox -->
                        <div class="col-auto d-flex align-items-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="myCheckbox">

                                <script>
                                    let showPasswordCheckbox = document.getElementById("myCheckbox");
                                    let passwordInput = document.getElementById("password");

                                    showPasswordCheckbox.addEventListener('change', function() {
                                        if (this.checked) {
                                            passwordInput.setAttribute("type", "text");
                                        } else {
                                            passwordInput.setAttribute("type", "password");
                                        }
                                    });
                                </script>

                                <label class="custom-control-label" for="myCheckbox">Show password</label>
                            </div>
                        </div>
                    </div>


                    <!-- Login Button -->
                    <div class="mb-3">
                        <button type="submit" name="submit" class="btn btn-block">Login</button>
                    </div>

                    <!-- Forget Password -->
                    <div class="text-right">
                        <a href="index.jsp" class="forget-link">Not an Admin ?</a>
                    </div>

                </form>
            </div>
            <!-- FORM END -->
        </div>
        <!-- FORM CONTAINER END -->
    </div>
</div>

</body>

</html>
