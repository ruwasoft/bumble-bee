<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Categories</title>
    <link rel="stylesheet" href="css/adminDashboardStyle.css">
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>


    <style>
        #categories {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #categories td, #categories th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #categories tr:nth-child(even){background-color: #f2f2f2;}

        #categories tr:hover {background-color: #ddd;}

        #categories th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #fcdc49;
            color: black;
        }

        <!-- -->

        label {
            font-weight: bold;
        }

        input[type=text] {
            width: 20%;
            padding: 12px 20px;
            margin: 8px 4px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 10%;
            background-color: black;
            color: #fbd31c;
            font-weight: bold;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #171717;
            color: white;
        }



    </style>


</head>

<body>

<div class="wrapper">

    <jsp:include page="AdminSideBar.jsp" />

    <div class="main_content">
        <div class="header">Categories</div>
        <div class="info">

            <!-- Show Categories list load from table and show text box and button to add new Category ----------->
            <div>


                <!--Form to add new category -->
                <form action="insertCategory" method="post">
                    <label for="categoryName">Category Name</label>
                    <input type="text" id="categoryName" name="categoryName" placeholder="Enter new category name here.." required maxlength="45" >

                    <input type="submit" value="Add">
                </form>
                <!--End of Form to add new category -->


                <!-- View categories Table here -->
                <table id="categories">

                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th> </th>
                    </tr>

                    <c:forEach var = "category" items="${categoryList}">


                        <c:set var="id" value="${category.ID}"/>
                        <c:set var="name" value="${category.name}"/>


                        <tr>
                            <td>${id}</td>
                            <td>${name}</td>
                            <td> <a href="deleteCategory?id=${id}" onclick="return confirm('Are you sure you want to delete the category \'${category.name}\' ?');" ><img src="img/delete.png" height="16" alt="Delete"></a> </td>
                        </tr>


                    </c:forEach>

                </table>
                <!-- End of View categories Table -->

            </div>
            <!--End of Showing Categories list load from table and show text box and button to add new Category -->

        </div>
    </div>

</div>





</body>

</html>






