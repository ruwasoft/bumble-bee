package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Common.Common;
import lk.icbt.bumblebee.Entity.Admin;
import lk.icbt.bumblebee.Entity.Customer;
import lk.icbt.bumblebee.Models.AdminDBUtill;
import lk.icbt.bumblebee.Models.CustomerDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminCustomersServlet", value = "/AdminCustomersServlet")
public class AdminCustomersServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        List<Customer> customerList = CustomerDBUtill.getAllCustomersDetails();
        request.setAttribute("customerList",customerList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminCustomers.jsp");
        requestDispatcher.forward(request,response);

    }
}
