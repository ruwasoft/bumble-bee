package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Common.Common;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Entity.Category;
import lk.icbt.bumblebee.Entity.Product;
import lk.icbt.bumblebee.Models.BrandsDBUtill;
import lk.icbt.bumblebee.Models.CategoryDBUtill;
import lk.icbt.bumblebee.Models.ProductsDBUtill;

import java.io.*;
import java.nio.file.Paths;
import java.util.List;

@WebServlet(name = "AdminEditProductsServlet", value = "/AdminEditProductsServlet")
@MultipartConfig
public class AdminEditProductsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Category> categoryList = CategoryDBUtill.getCategoryList();

        List<Brand> brandList = BrandsDBUtill.getBrandList();

        request.setAttribute("categoryList",categoryList);
        request.setAttribute("brandList",brandList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminEditProductPage.jsp");
        requestDispatcher.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String image="";

        //save image in file system and get the path to store in DB-----------------------------------------------------
        Part filePart = request.getPart("image"); // Retrieves <input type="file" name="image">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();
        byte[] buffer = new byte[fileContent.available()];
        fileContent.read(buffer);

       if(fileName== "")
       {
            image = request.getParameter("currentImage");
       }
       else
       {
           // Save the file to the file system---------------------------------------------------------------------------
           String folderToSaveImage = "/uploaded images/";

           File targetFile = new File(Common.serverPathToSaveImage+ folderToSaveImage+ fileName);
           OutputStream outStream = new FileOutputStream(targetFile);
           outStream.write(buffer);
           outStream.close();
           //Ending of save image in file system and get the path to store in DB-------------------------------------------

           image = folderToSaveImage+fileName;
       }


        String id = request.getParameter("productId");
        String name = request.getParameter("productName");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String cutPrice = request.getParameter("cutPrice");
        String mainCategory = request.getParameter("mainCategory");
        String brand = request.getParameter("brand");
        String availableQty = request.getParameter("availableQty");


        String buttonClicked = request.getParameter("submit"); //for check which button was clicked

        if (buttonClicked != null) // Update button was clicked,Handle the update operation here
        {

            boolean isTrue;
            isTrue = ProductsDBUtill.updateProduct(id,name,description,price,cutPrice,mainCategory,brand,image,availableQty);

            if(isTrue == true)
            {

                // Redirect to AdminProductsServlet
                response.sendRedirect(request.getContextPath() + "/Products");

            }
            else
            {
                response.setContentType("text/html");
                response.getWriter().write("<script>alert('An error has occurred');</script>");

                // Redirect to AdminProductsServlet
                response.sendRedirect(request.getContextPath() + "/Products");


            }

        }

        buttonClicked = request.getParameter("delete"); // Delete button was clicked, Handle the delete operation here
        if (buttonClicked != null)
        {

            boolean isTrue;
            isTrue = ProductsDBUtill.deleteProduct(id);

            if(isTrue)
            {

                // Redirect to AdminProductsServlet
                response.sendRedirect(request.getContextPath() + "/Products");

            }
            else
            {
                response.setContentType("text/html");
                response.getWriter().write("<script>alert('An error has occurred');</script>");

                // Redirect to AdminProductsServlet
                response.sendRedirect(request.getContextPath() + "/Products");


            }

        }










    }
}
