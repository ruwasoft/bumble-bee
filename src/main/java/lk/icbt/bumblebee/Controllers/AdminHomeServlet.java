package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Entity.Category;
import lk.icbt.bumblebee.Entity.Customer;
import lk.icbt.bumblebee.Entity.Product;
import lk.icbt.bumblebee.Models.BrandsDBUtill;
import lk.icbt.bumblebee.Models.CategoryDBUtill;
import lk.icbt.bumblebee.Models.CustomerDBUtill;
import lk.icbt.bumblebee.Models.ProductsDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminHomeServlet", value = "/AdminHomeServlet")
public class AdminHomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int productsCount = 0;
        int categoriesCount = 0;
        int brandsCount = 0;
        int customersCount = 0;


        List<Product> productList = ProductsDBUtill.getProductList();
        productsCount = productList.size();

        List<Category> categoryList = CategoryDBUtill.getCategoryList();
        categoriesCount = categoryList.size();

        List<Brand> brandList = BrandsDBUtill.getBrandList();
        brandsCount = brandList.size();

        List<Customer> customerList = CustomerDBUtill.getAllCustomersDetails();
        customersCount = customerList.size();


        request.setAttribute("productsCount",productsCount);
        request.setAttribute("categoriesCount",categoriesCount);
        request.setAttribute("brandsCount",brandsCount);
        request.setAttribute("customersCount",customersCount);


        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminHome.jsp");
        requestDispatcher.forward(request,response);

    }


}
