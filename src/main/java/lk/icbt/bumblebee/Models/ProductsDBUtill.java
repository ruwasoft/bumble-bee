package lk.icbt.bumblebee.Models;

import lk.icbt.bumblebee.Database.DBConnect;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Entity.Product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductsDBUtill {


    private static boolean isSuccess = false;
    private static Connection con = null;
    private  static Statement stmt = null;
    private static ResultSet rs= null;


    //Function use to get All products details--------------------------------------------------------------------------
    public static List<Product> getProductList()
    {
        ArrayList<Product> products = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TblProducts";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                int id = rs.getInt(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String cutPrice = rs.getString(5);
                String mainCategory = rs.getString(6);
                String brand = rs.getString(7);
                String image = rs.getString(8);
                String availableQty = rs.getString(9);


                Product product = new Product(id,name,description,price,cutPrice,mainCategory,brand,image,availableQty);

                products.add(product);

            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return products;

    }
    //End oif function use to get all Product details-------------------------------------------------------------------


    ///Function use to insert product-----------------------------------------------------------------------------------
    public static boolean insertProduct(String productName,String description,String price,String cutPrice,String mainCategory,String brand,String image,String availableQTY)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "INSERT INTO TblProducts VALUES (0,'"+productName+"','"+description+"','"+price+"','"+cutPrice+"','"+mainCategory+"','"+brand+"','"+image+"','"+availableQTY+"')";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return isSuccess;
    }
    ///End of Function use to insert product----------------------------------------------------------------------------


    ///function use to delete the product from db-----------------------------------------------------------------------
    public static boolean deleteProduct(String id)
    {
        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            int convertedID = Integer.parseInt(id);

            String sql = "DELETE FROM TblProducts  WHERE ID='"+convertedID+"' ";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  isSuccess;
    }
    ///End of function use to delete the product from db----------------------------------------------------------------


    ///Function use to update product-----------------------------------------------------------------------------------
    public static boolean updateProduct(String id,String productName,String description,String price,String cutPrice,String mainCategory,String brand,String image,String availableQTY)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "UPDATE TblProducts SET Name='"+productName+"',Description='"+description+"',Price='"+price+"',CuttedPrice='"+cutPrice+"',MainCategory='"+mainCategory+"',Brand='"+brand+"',Image='"+image+"',AvailableQty='"+availableQTY+"' WHERE Id = '"+id+"' ";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return isSuccess;
    }
    ///End of Function use to update product----------------------------------------------------------------------------

}
