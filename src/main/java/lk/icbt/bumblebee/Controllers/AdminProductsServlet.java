package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Product;
import lk.icbt.bumblebee.Models.BrandsDBUtill;
import lk.icbt.bumblebee.Models.ProductsDBUtill;


import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminProductsServlet", value = "/AdminProductsServlet")
public class AdminProductsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Product> productList = ProductsDBUtill.getProductList();
        request.setAttribute("productList",productList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminProducts.jsp");
        requestDispatcher.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String name = request.getParameter("Name");
        String description = request.getParameter("Description");
        String price = request.getParameter("Price");
        String cutPrice = request.getParameter("CutPrice");
        String mainCategory = request.getParameter("MainCategory");
        String brand = request.getParameter("Brand");
        String image = request.getParameter("Image");
        String availableQty = request.getParameter("AvailableQty");




        boolean isTrue;
        isTrue = ProductsDBUtill.insertProduct(name,description,price,cutPrice,mainCategory,brand,image,availableQty);

        if(isTrue == true)
        {
            doGet(request,response);
        }
        else
        {
            response.setContentType("text/html");
            response.getWriter().write("<script>alert('An error has occurred');</script>");

            doGet(request,response);
        }


    }
}
