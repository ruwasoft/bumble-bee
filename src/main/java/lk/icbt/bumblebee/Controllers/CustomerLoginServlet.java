package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Common.Common;
import lk.icbt.bumblebee.Entity.Admin;
import lk.icbt.bumblebee.Entity.Customer;
import lk.icbt.bumblebee.Models.AdminDBUtill;
import lk.icbt.bumblebee.Models.CustomerDBUtill;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "CustomerLoginServlet", value = "/CustomerLoginServlet")
public class CustomerLoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String email = request.getParameter("Email");
        String password = request.getParameter("Password");

        boolean isTrue = CustomerDBUtill.CustomerLogin(email,password);

        if(isTrue)
        {

            List<Customer> customerDetails = CustomerDBUtill.getCustomerDetails(email);
            request.setAttribute("customerDetails",customerDetails);


            //set values to global object for show in profile page-----------
            Common.customer.setID(customerDetails.get(0).getID());
            Common.customer.setName(customerDetails.get(0).getName());
            Common.customer.setEmail(customerDetails.get(0).getEmail());
            Common.customer.setNIC(customerDetails.get(0).getNIC());
            Common.customer.setAddress(customerDetails.get(0).getAddress());
            Common.customer.setPhone(customerDetails.get(0).getPhone());
            Common.customer.setDOB(customerDetails.get(0).getDOB());
            Common.customer.setLoanBalance(customerDetails.get(0).getLoanBalance());
            Common.customer.setInstallmentPlan(customerDetails.get(0).getInstallmentPlan());
            //End of setting values to global object for show in profile page


            // Redirect to AdminHomeServlet
            response.sendRedirect(request.getContextPath() + "/AllProducts");

        }
        else
        {

            // Send an err message as an alert box to the user
            out.println("<html>");
            out.println("<head>");
            out.println("<title>MyServlet</title>");
            out.println("<style>");
            out.println("  .alert {");
            out.println("    color: red;");
            out.println("  }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script>");
            out.println("alert('Username or password invalid!');");
            out.println("window.location.replace('customerLogin.jsp');");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");

        }

    }
}
