package lk.icbt.bumblebee.Models;

import lk.icbt.bumblebee.Database.DBConnect;
import lk.icbt.bumblebee.Entity.Category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CategoryDBUtill {


    private static boolean isSuccess = false;
    private static Connection con = null;
    private  static Statement stmt = null;
    private static ResultSet rs= null;


    //Function use to get All Category details---------------------------------------------------------------------------------
    public static List<Category> getCategoryList()
    {
        ArrayList<Category> categories = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TblMainCategories";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                int id = rs.getInt(1);
                String name = rs.getString(2);

                Category category = new Category(id,name);

                categories.add(category);

            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  categories;

    }
    //End oif function use to get Category details-------------------------------------------------------------------------


    ///Function use to insert-------------------------------------------------------------------------------------------
    public static boolean insertCategory(String categoryName)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "INSERT INTO TblMainCategories VALUES (0,'"+categoryName+"')";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return isSuccess;
    }
    ///End of Function use to insert------------------------------------------------------------------------------------


    ///function use to delete the category from db----------------------------------------------------------------------
    public static boolean deleteCategory(String id)
    {
        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            int convertedID = Integer.parseInt(id);

            String sql = "DELETE FROM TblMainCategories  WHERE ID='"+convertedID+"' ";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  isSuccess;
    }
    ///End of function use to delete the category from db---------------------------------------------------------------


}
