package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Common.Common;
import lk.icbt.bumblebee.Entity.Admin;
import lk.icbt.bumblebee.Models.AdminDBUtill;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "AdminLoginServlet", value = "/AdminLoginServlet")
public class AdminLoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String email = request.getParameter("Email");
        String password = request.getParameter("Password");

        boolean isTrue = AdminDBUtill.AdminLogin(email,password);

        if(isTrue)
        {

            List<Admin> adminDetails = AdminDBUtill.getAdminDetails(email);
            request.setAttribute("adminDetails",adminDetails);


            //set values to global object for show in profile page-----------
            Common.admin.setEmail(adminDetails.get(0).getEmail());
            Common.admin.setName(adminDetails.get(0).getName());
            Common.admin.setPhone(adminDetails.get(0).getPhone());
            //End of setting values to global object for show in profile page


            // Redirect to AdminHomeServlet
            response.sendRedirect(request.getContextPath() + "/Home");

        }
        else
        {

            // Send a err message as an alert box to the user
            out.println("<html>");
            out.println("<head>");
            out.println("<title>MyServlet</title>");
            out.println("<style>");
            out.println("  .alert {");
            out.println("    color: red;");
            out.println("  }");
            out.println("</style>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script>");
            out.println("alert('Username or password invalid!');");
            out.println("window.location.replace('AdminLogin.jsp');");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");

        }

    }
}
