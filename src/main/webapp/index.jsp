
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>
    <title>Bumble Bee</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <!-- IMAGE CONTAINER BEGIN -->
        <div class="col-lg-6 col-md-6 d-none d-md-block infinity-image-container2">

            <br> <br>

            <h2>Buy first and pay later..</h2>

            <br>

            <img src="img/bg2.png" height="700">

        </div>
        <!-- IMAGE CONTAINER END -->

        <!-- FORM CONTAINER BEGIN -->
        <div class="col-lg-6 col-md-6 infinity-form-container">
            <div class="col-lg-9 col-md-12 col-sm-8 col-xs-12 infinity-form">
                <!-- Company Logo -->
                <div class="text-center mb-3 mt-5">
                    <img src="img/logo.png" width="150px">
                </div>
                <div class="text-center mb-4">
                    <h4>Register here</h4>
                </div>

                <!-- Form -->
                <form action="Register" method="post" class="px-3" onsubmit="return validatePasswords()">


                    <div class="form-input">
                        <span><i class="fa fa-user"></i></span>
                        <input type="text" name="name" placeholder="Name" tabindex="10" required>
                    </div>


                    <div class="form-input">
                        <span><i class="fa fa-envelope"></i></span>
                        <input type="email" name="email" placeholder="Email Address" tabindex="10"required>
                    </div>


                    <div class="form-input">
                        <span><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" id="password" placeholder="Password" required>
                    </div>


                    <div class="form-input">
                        <span><i class="fa fa-lock"></i></span>
                        <input type="password" name="ConfirmPassword" id="confirmPassword" placeholder="Retype Password" required>
                    </div>



                    <div class="form-input">
                        <span><i class="fa fa-id-card"></i></span>
                        <input type="text" name="nic" placeholder="NIC Number" tabindex="10" required>
                    </div>



                    <div class="form-input">
                        <span><i class="fa fa-map-marker"></i></span>
                        <input type="text" name="address" placeholder="Address" tabindex="10" required>
                    </div>



                    <div class="form-input">
                        <span><i class="fa fa-phone"></i></span>
                        <input type="text" name="phone" placeholder="Mobile Number" tabindex="10" required>
                    </div>



                    <div class="form-input">
                        <span><i class="fa fa-calendar"></i></span>
                        <input type="text" name="DOB" placeholder="Birthday" tabindex="10" required>
                    </div>





                    <!-- Register Button -->
                    <div class="mb-3">
                        <button type="submit" name="submit" class="btn btn-block">Register</button>
                    </div>


                    <div class="row">

                        <div class="col-md-6 text-left">
                            <a href="customerLogin.jsp" class="forget-link">Already have an account ?</a>
                        </div>

                        <div class="col-md-6 text-right">
                            <a href="AdminLogin.jsp" class="forget-link">Are you an admin ?</a>
                        </div>

                    </div>


                </form>


                <script>
                    function validatePasswords() {
                        let password = document.getElementById("password").value;
                        let confirmPassword = document.getElementById("confirmPassword").value;

                        if (password !== confirmPassword) {
                            alert("Passwords do not match!");
                            return false;
                        }

                        return true;
                    }
                </script>


            </div>
            <!-- FORM END -->
        </div>
        <!-- FORM CONTAINER END -->
    </div>
</div>

</body>

</html>
