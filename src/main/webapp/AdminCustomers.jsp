<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <title>Customers</title>
  <link rel="stylesheet" href="css/adminDashboardStyle.css">
  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>

  <style>
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td, #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #fcdc49;
      color: black;
    }
  </style>

</head>

<body>

<div class="wrapper">

  <jsp:include page="AdminSideBar.jsp" />

  <div class="main_content">
    <div class="header">Customers</div>
    <div class="info">

      <div>

        <!-- View Customers Table here -->

        <table id="customers">

          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>NIC</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Birth day</th>
            <th>Loan balance</th>
            <th>Installment plan</th>
          </tr>

          <c:forEach var = "cus" items="${customerList}">


            <c:set var="id" value="${cus.ID}"/>
            <c:set var="name" value="${cus.name}"/>
            <c:set var="email" value="${cus.email}"/>
            <c:set var="nic" value="${cus.NIC}"/>
            <c:set var="address" value="${cus.address}"/>
            <c:set var="phone" value="${cus.phone}"/>
            <c:set var="dob" value="${cus.DOB}"/>
            <c:set var="loanBalance" value="${cus.loanBalance}"/>
            <c:set var="installmenPlan" value="${cus.installmentPlan}"/>

            <tr>
              <td>${id}</td>
              <td>${name}</td>
              <td>${email}</td>
              <td>${nic}</td>
              <td>${address}</td>
              <td>${phone}</td>
              <td>${dob}</td>
              <td>${loanBalance}</td>
              <td>${installmenPlan}</td>
            </tr>


          </c:forEach>

        </table>

        <!-- End of View Customers Table -->

      </div>

    </div>
  </div>

</div>





</body>
</html>






