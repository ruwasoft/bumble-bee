<%@ page import="lk.icbt.bumblebee.Common.Common" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <title>Profile</title>
  <link rel="stylesheet" href="css/adminDashboardStyle.css">
  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>

  <style>
    .card {
      border: 1px solid #ccc;
      border-radius: 10px;
      padding: 20px;
      display: flex;
      flex-direction: column;
      align-items: center;
      position: relative;
    }

    .card::before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 10px;
      background-color: #fbd31c;
      border-top-left-radius: 10px;
      border-top-right-radius: 10px;
    }

    .card img {
      border-radius: 50%;
      margin-bottom: 10px;
    }
  </style>

</head>

<body>

<div class="wrapper">

  <jsp:include page="CustomerSideBar.jsp" />

  <div class="main_content">

    <div class="header">Profile</div>

    <div class="info">

      <div class="card">
        <img src="img/admin.png" alt="User" width="100" height="100">
        <div><b>Name:</b> <%= Common.customer.getName() %></div>
        <div><b>Email:</b> <%= Common.customer.getEmail() %></div>
        <div><b>Phone:</b> <%= Common.customer.getPhone() %></div>
        <div><b>NIC:</b> <%= Common.customer.getNIC() %></div>
        <div><b>Address:</b> <%= Common.customer.getAddress() %></div>
        <div><b>Birth day:</b> <%= Common.customer.getDOB() %></div>
        <div><b>Loan Balance:</b> <%= Common.customer.getLoanBalance() %></div>
        <div><b>Installment plan:</b> <%= Common.customer.getInstallmentPlan() %></div>

      </div>

    </div>

  </div>

</div>





</body>
</html>






