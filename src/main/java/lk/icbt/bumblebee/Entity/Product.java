package lk.icbt.bumblebee.Entity;

public class Product {

    private int ID;
    private String Name;
    private String Description;

    private String Price;
    private String CuttedPrice;
    private String MainCategory;
    private String Brand;
    private String Image;

    private String AvailableQty;


    public Product() {
    }


    public Product(int ID, String name, String description, String price, String cuttedPrice, String mainCategory, String brand, String image, String availableQty) {
        this.ID = ID;
        Name = name;
        Description = description;
        Price = price;
        CuttedPrice = cuttedPrice;
        MainCategory = mainCategory;
        Brand = brand;
        Image = image;
        AvailableQty = availableQty;
    }


    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getPrice() {
        return Price;
    }

    public String getCuttedPrice() {
        return CuttedPrice;
    }

    public String getMainCategory() {
        return MainCategory;
    }

    public String getBrand() {
        return Brand;
    }

    public String getImage() {
        return Image;
    }

    public String getAvailableQty() {
        return AvailableQty;
    }


    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public void setCuttedPrice(String cuttedPrice) {
        CuttedPrice = cuttedPrice;
    }

    public void setMainCategory(String mainCategory) {
        MainCategory = mainCategory;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public void setImage(String image) {
        Image = image;
    }

    public void setAvailableQty(String availableQty) {
        AvailableQty = availableQty;
    }
}
