package lk.icbt.bumblebee.Controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lk.icbt.bumblebee.Entity.Brand;
import lk.icbt.bumblebee.Entity.Customer;
import lk.icbt.bumblebee.Models.BrandsDBUtill;
import lk.icbt.bumblebee.Models.CustomerDBUtill;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminBrandsServlet", value = "/AdminBrandsServlet")
public class AdminBrandsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Brand> brandList = BrandsDBUtill.getBrandList();
        request.setAttribute("brandList",brandList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminBrands.jsp");
        requestDispatcher.forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String brandName = req.getParameter("brandName");

        boolean isTrue;
        isTrue = BrandsDBUtill.insertBrand(brandName);

        if(isTrue == true)
        {
            // Redirect to AdminBrandsServlet
            resp.sendRedirect(req.getContextPath() + "/Brands");
        }
        else
        {
            resp.setContentType("text/html");
            resp.getWriter().write("<script>alert('An error has occurred');</script>");

            // Redirect to AdminBrandsServlet
            resp.sendRedirect(req.getContextPath() + "/Brands");

        }

    }
}
