package lk.icbt.bumblebee.Entity;

public class Admin {


    private String Email;
    private String Password;
    private String Name;
    private String Phone;


    public Admin(String email, String password, String name, String phone) {
        Email = email;
        Password = password;
        Name = name;
        Phone = phone;
    }


    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
