package lk.icbt.bumblebee.Models;

import lk.icbt.bumblebee.Database.DBConnect;
import lk.icbt.bumblebee.Entity.Admin;
import lk.icbt.bumblebee.Entity.Customer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerDBUtill {

    private static boolean isSuccess = false;
    private static Connection con = null;
    private  static Statement stmt = null;
    private static ResultSet rs= null;


    //Function use to login---------------------------------------------------------------------------------------------
    public static boolean CustomerLogin(String email, String password)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "SELECT * FROM TblCustomers WHERE Email='"+email+"' AND Password = '"+password+"' ";
            rs = stmt.executeQuery(sql);

            if(rs.next())
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


        return isSuccess;

    }
    //End of function use to login--------------------------------------------------------------------------------------



    //Function use to get All Customers details---------------------------------------------------------------------------------
    public static List<Customer> getAllCustomersDetails()
    {
        ArrayList<Customer> customers = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TblCustomers";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                int id = rs.getInt(1);
                String name = rs.getString(2);
                String email = rs.getString(3);
                String nic = rs.getString(5);
                String address = rs.getString(6);
                String phone = rs.getString(7);
                String dob = rs.getString(8);
                String loanBalance = rs.getString(9);
                String installmentPlan = rs.getString(10);


                Customer customer = new Customer(id,name,email,"",nic,address,phone,dob,loanBalance,installmentPlan);

                customers.add(customer);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  customers;

    }
    //End oif function use to get Customer details-------------------------------------------------------------------------



    ///Function use to insert new Customer-------------------------------------------------------------------------------------------
    public static boolean insertCustomer(Customer customer)
    {

        try
        {

            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "INSERT INTO TblCustomers VALUES (0,'"+customer.getName()+"','"+customer.getEmail()+"','"+customer.getPassword()+"','"+customer.getNIC()+"','"+customer.getAddress()+"','"+customer.getPhone()+"','"+customer.getDOB()+"','"+customer.getLoanBalance()+"','"+customer.getInstallmentPlan()+"' )";

            int rs = stmt.executeUpdate(sql);

            if(rs>0)
            {
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        return isSuccess;
    }
    ///End of Function use to insert new customer------------------------------------------------------------------------------------


    //Function use to get logged customer details---------------------------------------------------------------------------------
    public static List<Customer> getCustomerDetails(String email)
    {
        ArrayList<Customer> customers = new ArrayList<>();

        try
        {
            con = DBConnect.getConnection();
            stmt = con.createStatement();

            String sql = "select * from TBLCustomers where Email= '"+email+"' ";
            rs = stmt.executeQuery(sql);

            while (rs.next())
            {

                int id = rs.getInt(1);
                String name = rs.getString(2);
                String nic = rs.getString(5);
                String address = rs.getString(6);
                String phone = rs.getString(7);
                String dob = rs.getString(8);
                String loanBalance = rs.getString(9);
                String installmentPlan = rs.getString(10);


                Customer customer = new Customer(id,name,email,"",nic,address,phone,dob,loanBalance,installmentPlan);

                customers.add(customer);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return  customers;

    }
    //End oif function use to get logged customer details-------------------------------------------------------------------------

}
