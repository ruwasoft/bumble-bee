package lk.icbt.bumblebee.Entity;

public class Customer {

    private int ID;
    private String Name;
    private String Email;
    private String Password;
    private String NIC;
    private String Address;
    private String Phone;
    private String DOB;
    private String LoanBalance;

    private String InstallmentPlan;

    public Customer() {
    }


    public Customer(int ID, String name, String email, String password, String NIC, String address, String phone, String DOB, String loanBalance, String installmentPlan) {
        this.ID = ID;
        Name = name;
        Email = email;
        Password = password;
        this.NIC = NIC;
        Address = address;
        Phone = phone;
        this.DOB = DOB;
        LoanBalance = loanBalance;
        InstallmentPlan = installmentPlan;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }

    public String getNIC() {
        return NIC;
    }

    public String getAddress() {
        return Address;
    }

    public String getPhone() {
        return Phone;
    }

    public String getDOB() {
        return DOB;
    }

    public String getLoanBalance() {
        return LoanBalance;
    }

    public String getInstallmentPlan() {
        return InstallmentPlan;
    }


    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void setNIC(String NIC) {
        this.NIC = NIC;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public void setLoanBalance(String loanBalance) {
        LoanBalance = loanBalance;
    }

    public void setInstallmentPlan(String installmentPlan) {
        InstallmentPlan = installmentPlan;
    }
}
